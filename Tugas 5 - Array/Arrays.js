/* 
 Buatlah sebuah variable dengan nama nomorGenap yang merupakan sebuah array dengan ketentuan: 
  - Array tersebut menampung bilangan genap dari 1 hingga 100

  Note: 
    - Agar lebih mudah bisa menggunakan for loop dan logika if untuk mengisi array tersebut.
*/
let number = 100;
function nomorGenap() {
  //Isi Kode di sini....
  var arrGenap = [];
  for (i = 1; i <= number; i++) {
    if (i % 2 == 0) {
      arrGenap.push(i);
      i++;
    }
  }
  return arrGenap;
}
console.log(nomorGenap());
//Hiraukan kode di bawah ini
module.exports = { nomorGenap };

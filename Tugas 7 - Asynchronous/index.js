const { resolve } = require("path");
let readBooksPromise = require("./promise.js");

let books = [
  { name: "LOTR", timeSpent: 3000 },
  { name: "Fidas", timeSpent: 2000 },
  { name: "Kalkulus", timeSpent: 4000 },
];

/*
Lanjutkan code pada index.js untuk memanggil function readBooksPromise. Buku yang akan dihabiskan adalah buku-buku di dalam array books.
Pertama function readBooksPromise menerima input waktu yang dimiliki yaitu 10000ms (10 detik) dan books pada indeks ke-0.
Setelah mendapatkan callback sisa waktu yang dikirim lewat callback, sisa waktu tersebut dipakai untuk membaca buku pada indeks ke-1.
Begitu seterusnya sampai waktu habis atau semua buku sudah terbaca.
*/

// Tulis code untuk memanggil function readBooksPromise di sini
readBooksPromise(10000, books[0])
  .then((waktu) => {
    readBooksPromise(waktu, books[1])
      .then((waktu) => {
        readBooksPromise(waktu, books[2]).catch((reason) => {
          console.log(`Gagal, waktu ${reason} tidak cukup`);
        });
      })
      .catch((reason) => {
        console.log(`Gagal, waktu ${reason} tidak cukup`);
      });
  })
  .catch((reason) => {
    console.log(`Gagal, waktu ${reason} tidak cukup`);
  });

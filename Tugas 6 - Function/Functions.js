/*
1. Buatlah sebuah fungsi bernama minimal dengan ketentuan berikut: 
  - Menerima dua buah argumen dengan tipe data number, yang bernama a dan b.
  - Return nilai terkecil antara a atau b.
  - Bila nilai keduanya sama, maka return dengan nilai a.

  contoh:
    minimal(1,4) //Output: 1
    minimal(3,2) //Output: 2
    minimal(3,3) //Output: 3

2. Buatlah sebuah fungsi bernama kalikan dengan ketentuan berikut:
  - Menerima dua buah argumen dengan tipe data number, yang bernama a dan b.
  - Return hasil dari perkalian antara a dan b.
*/

function minimal(a, b) {
  return Math.min(a, b);
}
console.log(minimal(1, 4));
console.log(minimal(3, 2));
console.log(minimal(3, 3));

function kalikan(a, b) {
  return a * b;
}
console.log(kalikan(5, 3));
//Hiraukan kode di bawah ini
module.exports = { kalikan, minimal };

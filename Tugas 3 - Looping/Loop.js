/*
Soal No. 1 Looping While
Anda diminta untuk membuat looping dengan while supaya output yang dihasilkan :

LOOPING PERTAMA
2 - I love coding
4 - I love coding
6 - I love coding
8 - I love coding
10 - I love coding
12 - I love coding
14 - I love coding
16 - I love coding
18 - I love coding
20 - I love coding

*/

// ============================================================================

/*
Soal No.2 Looping menggunakan for
Buatlah looping dengan syarat: 
  - Jika angkanya ganjil maka tampilkan "Semangat"
  - Jika angkanya genap maka tampilkan "Berkualitas"
  - Jika angka yang sedang ditampilkan adalah kelipatan 3 DAN angka ganjil maka tampilkan "I Love Coding"
*/
/* var i = 1;
while (i <= 20) {
  if (i % 2 == 0) {
    var text = i + " - I love coding";
    console.log(text);
  }
  i++;
}
*/

// Soal no 2
var number = 20;
for (i = 1; i <= number; i++) {
  if (i % 3 == 0 && i % 2 != 0) {
    console.log(i + " I Love Coding");
  } else if (i % 2 == 0) {
    console.log(i + " Berkualitas");
  } else if (i % 2 != 0) {
    console.log(i + " Semangat");
  } else {
    console.log(i);
  }
}
